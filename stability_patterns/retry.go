package stability_patterns

import (
	"context"
	"log"
	"time"
)

func Retry(effector Effector, retries uint, delay time.Duration) Effector {
	return func(ctx context.Context) (string, error) {
		var r uint = 0
		for ; ; r++ {
			response, err := effector(ctx)
			if err != nil || r >= retries {
				return response, err
			}
			log.Printf("Attempt %d failed; retrying in %v", r+1, delay)
			select {
			case <-time.After(delay):
			case <-ctx.Done():
				return "", ctx.Err()
			}
		}
	}
}
