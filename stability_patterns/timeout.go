package stability_patterns

import (
	"context"
	"fmt"
	"time"
)

func Timeout(f SlowFunction) WithContext {
	return func(ctx context.Context, arg string) (string, error) {
		chres := make(chan string)
		cherr := make(chan error)

		go func() {
			res, err := f(arg)
			chres <- res
			cherr <- err
		}()

		select {
		case res := <-chres:
			return res, <-cherr
		case <-ctx.Done():
			return "", ctx.Err()
		}
	}
}

func TimeoutImplementation() {
	ctx := context.Background()
	ctxWithTimeout, cancel := context.WithTimeout(ctx, 1*time.Second)
	defer cancel()
	timeout := Timeout(func(s string) (string, error) {
		<-time.After(3 * time.Second)
		return "ready", nil
	})
	rs, err := timeout(ctxWithTimeout, "some input")
	fmt.Println(rs, err)
}
