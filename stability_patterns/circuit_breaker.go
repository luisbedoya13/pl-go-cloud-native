package stability_patterns

import (
	"context"
	"errors"
	"sync"
	"time"
)

func Breaker(circuit Circuit, failureThreshold uint8) Circuit {
	var consecutiveFailures int = 0
	var lastAttempt = time.Now()
	var m sync.RWMutex

	return func(ctx context.Context) (string, error) {
		m.RLock() // establish a "read lock"
		d := consecutiveFailures - int(failureThreshold)
		if d >= 0 {
			shouldRetryAt := lastAttempt.Add(time.Second * 2 << d) // (2 seconds)^d
			if !time.Now().After(shouldRetryAt) {
				m.RUnlock()
				return "", errors.New("service unreachable")
			}
		}
		m.RUnlock()                   // release read lock
		response, err := circuit(ctx) // issue request properly
		m.Lock()                      // lock around shared resources
		defer m.Unlock()
		lastAttempt = time.Now() // record time of attempt
		if err != nil {          // Circuit returned and error
			consecutiveFailures++ // so it count the failure
			return response, err  // and return
		}
		consecutiveFailures = 0 // reset failures counter
		return response, nil
	}
}
