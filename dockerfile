from golang:1.16 as build
copy . /src
workdir /src
run CGO_ENABLED=0 GOOS=linux go build -o kvs
from scratch
copy --from=build /src/kvs .
expose 8080
cmd ["/kvs"]