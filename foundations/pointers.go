package foundations

import "fmt"

func update(m map[rune]int8) {
	m['c'] = 3
}

func ByReference() {
	m := map[rune]int8{'a': 1, 'b': 2}
	update(m)
	fmt.Printf("%v\n", m)
}
