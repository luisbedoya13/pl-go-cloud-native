package foundations

import "fmt"

type Vertex struct {
	x, y float32
}

func Struct() {
	var v Vertex
	fmt.Printf("a struct is never nil %v\n", v)

	v = Vertex{}
	fmt.Printf("explicitly define an empty struct %v\n", v)

	v = Vertex{1.0, 2.0}
	fmt.Printf("init fields in order %v\n", v)

	v = Vertex{x: 2.0, y: 3.0}
	fmt.Printf("define fields by label %v\n", v)

	var v2 *Vertex = &Vertex{1, 3}
	fmt.Printf("accessing fields of pointer %v\n", v2.x)

	v2.x, v2.y = v2.y, v2.x
	fmt.Println(v2)
}
