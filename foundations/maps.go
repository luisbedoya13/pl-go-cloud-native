package foundations

import "fmt"

func Maps() {
	freezing := make(map[string]float32)
	freezing["Celsius"] = 0.0
	freezing["Fahrenheit"] = 32.0
	freezing["Kelvin"] = 273.2

	fmt.Println(freezing["kelvin"])
	fmt.Println(len(freezing))

	delete(freezing, "kelvin")
	fmt.Println(len(freezing))

	foo := freezing["no-such-key"] // Get non-existent key
	fmt.Println(foo)               // "0" (float32 zero value)

	newton, ok := freezing["newton"]
	fmt.Println(newton) // "0"
	fmt.Println(ok)     // "false"
}
