package foundations

import (
	"fmt"
)

func Strings() {
	s := "foö"
	r := []rune(s)
	b := []byte(s)

	fmt.Printf("%T %v\n", s, s)
	fmt.Printf("%T %v\n", r, r)
	fmt.Printf("%T %v\n", b, b)
}
