package foundations

import "fmt"

func product(factors ...int) int {
	p := 1
	for _, value := range factors { // factors is slice of int
		p *= value
	}
	return p
}

func VariadicFunction() {
	fmt.Println(product(5, 6, 7))
	s := []int{2, 3, 4}
	fmt.Println(product(s...)) // translate slice into variadic
}
