package foundations

import (
	"fmt"
	"time"
)

func SwitchFallthrough() {
	i := 0
	switch i % 3 {
	case 0:
		fmt.Println("Zero")
		fallthrough
	case 1:
		fmt.Println("One")
	case 2:
		fmt.Println("Two")
	default:
		fmt.Println("Huh?")
	}
}

func SwitchStatement() {
	switch hour := time.Now().Hour(); { // Empty expression means "true"
	case hour >= 6 && hour < 8:
		fmt.Println("I'm studying")
	case hour >= 8 && hour < 17:
		fmt.Println("I'm working")
	default:
		fmt.Println("I'm free")
	}
}
