package foundations

import (
	"fmt"
	"math"
)

type Shape interface {
	Area() float64
}

type Circle struct {
	radius float64
}

func (c Circle) Area() float64 {
	return c.radius * c.radius * math.Pi
}

func printArea(s Shape) {
	fmt.Printf("%T's area is %0.2f\n", s, s.Area())
}

func Interfaces() {
	circle := &Circle{5}
	printArea(circle)
}
