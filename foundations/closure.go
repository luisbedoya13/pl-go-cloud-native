package foundations

import "fmt"

func incrementer() func() int {
	a := 0
	return func() int {
		a++
		return a
	}
}

func Closure() {
	increment := incrementer()
	fmt.Println(increment())
	fmt.Println(increment())
	fmt.Println(increment())

	newIncrementer := incrementer()
	fmt.Println(newIncrementer())
}
