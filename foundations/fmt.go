package foundations

import "fmt"

func Formatting() {
	a := 17
	fmt.Printf("value in default format %v\n", a)
	fmt.Printf("representation of type %T\n", a)
	fmt.Printf("literal percent symbol %%\n")
	fmt.Printf("boolean (word true or false) %t\n", true)
	fmt.Printf("integer base 2 %b\n", a)
	fmt.Printf("integer base 10 %d\n", a)
	fmt.Printf("decimal point (no exponent) %f\n", 123.45)
	fmt.Printf("uninterpreted bytes of string %s\n", "hello")
	fmt.Printf("double-quoted string %q\n", "world")
}
