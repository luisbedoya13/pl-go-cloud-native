package foundations

import "fmt"

func (v *Vertex) Square() {
	v.x *= v.x
	v.y *= v.y
}

func Methods() {
	vert := &Vertex{2, 3}
	fmt.Println(vert)

	vert.Square()
	fmt.Println(vert)
}
