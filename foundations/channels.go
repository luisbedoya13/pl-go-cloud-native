package foundations

import (
	"fmt"
	"time"
)

func BlockingChannels() {
	ch := make(chan string)

	go func() {
		message := <-ch
		fmt.Println(message)
		ch <- "pong"
	}()

	ch <- "ping"
	fmt.Println(<-ch)
}

func BufferedChannels() {
	ch := make(chan string, 10)
	ch <- "foo"
	close(ch)

	msg, ok := <-ch
	fmt.Printf("%q %v\n", msg, ok) // "foo" true

	msg, ok = <-ch
	fmt.Printf("%q %v\n", msg, ok) // "" false
}

func LoopingOverChannels() {
	ch := make(chan byte, 3)

	ch <- 'a'
	ch <- 'b'
	ch <- 'c'

	close(ch)

	for s := range ch { // loops until channel is closed
		fmt.Printf("%q\n", s)
	}
}

func SelectOnChannels() {
	ch1 := make(chan uint8)
	ch2 := make(chan rune)
	ch3 := make(chan string)

	go func() {
		time.Sleep(time.Millisecond * 2500)
		ch1 <- 5
	}()

	go func() {
		time.Sleep(time.Second * 2)
		ch2 <- 'ü'
	}()

	go func() {
		word := <-ch3
		fmt.Printf("%v said: ", word)
	}()

	for {
		select {
		case <-ch1:
			fmt.Println("Good bye")
			return
		case a := <-ch2:
			fmt.Printf("His favorite letter is %q\n", a)
		case ch3 <- "Pedro":
			fmt.Println("Hello world")
		}
	}
}

func SelectOnTimeout() {
	ch1 := make(chan uint8)

	select {
	case m := <-ch1:
		fmt.Printf("Got %v\n", m)
	case <-time.After(3 * time.Second):
		fmt.Println("Timed out")
	}
}
