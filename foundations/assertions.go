package foundations

import "fmt"

func TypeAssertions() {
	var s Shape = nil // s is an expression of Shape
	s = Circle{}
	c := s.(Circle)       // assert that s is a Circle
	fmt.Printf("%T\n", c) // "foo.Circle"
}
