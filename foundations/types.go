package foundations

import "fmt"

type MyMap map[rune]uint

func (m MyMap) Length() int {
	return len(m)
}

func Types() {
	mm := MyMap{'a': 1, 'b': 2}
	fmt.Println(mm)
	mm['d'] = 4
	fmt.Println(mm.Length())
}
