package service

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

const connStrFormat = "host=%s dbname=%s user=%s password=%s"

type PostgresTransactionLogger struct {
	events chan<- Event
	errors <-chan error
	db     *sql.DB
}

func (l *PostgresTransactionLogger) WritePut(key, value string) {
	l.events <- Event{EventType: EventPut, Key: key, Value: value}
}

func (l *PostgresTransactionLogger) WriteDelete(key string) {
	l.events <- Event{EventType: EventDelete, Key: key}
}

func (l *PostgresTransactionLogger) Err() <-chan error {
	return l.errors
}

func (l *PostgresTransactionLogger) ReadEvents() (<-chan Event, <-chan error) {
	outEvent := make(chan Event)
	outError := make(chan error, 1)
	go func() {
		defer close(outEvent)
		defer close(outError)
		query := `
			SELECT sequence, event_type, key, value
			FROM transactions
			ORDER BY sequence
		`
		rows, err := l.db.Query(query)
		if err != nil {
			outError <- fmt.Errorf("sql query error: %w", err)
			return
		}
		defer rows.Close()
		e := Event{}
		for rows.Next() {
			err = rows.Scan(&e.Sequence, &e.EventType, &e.Key, &e.Value)
			if err != nil {
				outError <- fmt.Errorf("error reading row: %w", err)
				return
			}
			outEvent <- e
		}
		err = rows.Err()
		if err != nil {
			outError <- fmt.Errorf("transaction log read failure: %w", err)
		}
	}()
	return outEvent, outError
}

func (l *PostgresTransactionLogger) Run() {
	events := make(chan Event, 16)
	l.events = events
	errors := make(chan error, 1)
	l.errors = errors
	go func() {
		query := `
			INSERT INTO transactions
			(event_type, key, value) VALUES ($1, $2, $3)
		`
		for e := range events {
			_, err := l.db.Exec(query, e.EventType, e.Key, e.Value)
			if err != nil {
				errors <- err
			}
		}
	}()
}

func (l *PostgresTransactionLogger) verifyTableExists() (bool, error) {
	var result string
	rows, err := l.db.Query("SELECT to_regclass('public.transactions');")
	if err != nil {
		return false, err
	}
	for rows.Next() && result != "transactions" {
		rows.Scan(&result)
	}
	err = rows.Close()
	if err != nil {
		return false, err
	}
	return result == "transactions", rows.Err()
}

func (l *PostgresTransactionLogger) createTable() error {
	var err error
	createQuery := `
		CREATE TABLE transactions (
			sequence      BIGSERIAL PRIMARY KEY,
			event_type    SMALLINT,
			key 		  		TEXT,
			value         TEXT
		);
	`
	_, err = l.db.Exec(createQuery)
	if err != nil {
		return err
	}
	return nil
}

type PostgresDBParams struct {
	dbName   string
	host     string
	user     string
	password string
}

func NewPostgresTransactionLogger(c PostgresDBParams) (TransactionLogger, error) {
	connStr := fmt.Sprintf(connStrFormat, c.host, c.dbName, c.user, c.password)
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return nil, fmt.Errorf("failed to open db: %w", err)
	}
	err = db.Ping()
	if err != nil {
		return nil, fmt.Errorf("failed to open db connection: %w", err)
	}
	logger := &PostgresTransactionLogger{db: db}
	exists, err := logger.verifyTableExists()
	if err != nil {
		return nil, fmt.Errorf("failed to verify if table exists: %w", err)
	}
	if !exists {
		if err = logger.createTable(); err != nil {
			return nil, fmt.Errorf("failed to create table: %w", err)
		}
	}
	return logger, nil
}
